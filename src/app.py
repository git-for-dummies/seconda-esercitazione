from flask import Flask, abort, render_template
import pandas as pd


menu_item = '''<li class="nav-item{active}">
                    <a class="nav-link" href="{link}">{title}</a>
                </li>'''

song_item = '''<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between">
      <h5 class="mb-1">{title}</h5>
      <small>{year}</small>
    </div>
    <p class="mb-1">{artist}</p>
  </a>'''

pages = {
    "home": {
        "title": "Home",
        "content": "This is home"
    },
    "playlist": {
        "title": "Playlist",
        "content": "This is playlist"
    },
    "about": {
        "title": "About Us",
        "content": "This is about"
    }
}


def make_playlist():
    songs = []
    html_songs = ""
    try:
        with open('./static/playlist.md') as file:
            songs = file.read().splitlines()
    except:
        pass
    if songs == []:
        html_songs = '''<div class="alert alert-danger" role="alert">
  The file <span class="text-monospace text-secondary">playlist.md</span> is missing or is empty
</div>'''
    for song in songs:
        parsed_song = song.split('-')
        if len(parsed_song) != 3:
            continue
        html_songs += song_item.format(**{"title": parsed_song[0], "artist": parsed_song[1], "year": parsed_song[2]})
    return '<div class="list-group">' + html_songs + '</div>'


def make_menu(pages, current):
    menu_content = []
    for page in pages.keys():
        sub = {}
        sub["active"] = " active" if page == current else ""
        sub["link"] = "/{}".format(page) if page != current else "#"
        sub["title"] = pages[page]["title"]
        menu_content.append(menu_item.format(**sub))
    return "\n".join(menu_content)        


app = Flask(__name__)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', menu=make_menu(pages, '404')), 404

@app.route('/')
@app.route('/home')
def render_home():
    return render_template('home.html', menu=make_menu(pages, 'home'))

@app.route('/playlist')
def render_playlist():
    return render_template('playlist.html', menu=make_menu(pages, 'playlist'), playlist=make_playlist())

@app.route('/about')
def render_about():
    return render_template('about.html', menu=make_menu(pages, 'about'))
